const Telegraf = require('telegraf');
const helpers = require('./helpers.js');

require('dotenv').config(); // for apikeys

const bot = new Telegraf(process.env.APIKEY_TELEGRAM);

bot.command(helpers.appendName(['start']), ({ reply }) => reply('Sasehashs fantastical Bot (aka MensaBot). Find me on https://gitlab.com/samsumas/sasehashsbot.\n\n ... btw I use Arch ❤️'));

const activeModules = [
    require('./mensa.js'),
    require('./jokes.js'),
    require('./utilities.js'),
    require('./images.js'),
    require('./spellchecker.js'),
    require('./time'),
    require('./vaisselle.js'),
    require('./yolo.js'),
    require('./automod.js'),
];

/**
 * An inlineQueryHandler gets an ctx and returns a promise for the inline query reply.
 * Just add it in this list with registerQueryHandler(handlers).
 */
const inlineQueryHandlers = [];

activeModules.forEach((activeModule) => {
    if (activeModule.run !== undefined) {
        activeModule.run(bot);
    }
    if (activeModule.registerQueryHandler !== undefined) {
        activeModule.registerQueryHandler(inlineQueryHandlers);
    }
});

const applyHandlers = async ctx => Promise.all(inlineQueryHandlers.map(f => f(ctx))).then(ctx.answerInlineQuery);

bot.on('inline_query', applyHandlers);

bot.startPolling();
