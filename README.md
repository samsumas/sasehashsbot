# Installation

## Install dependencies:
```
git clone https://gitlab.com/samsumas/sasehashsbot
git submodule update --init
npm install
vim .env # or an other text editor
```

## Set environment in .env file

```
APIKEY_TELEGRAM=...
#the name you gave to your bot (for /...@BOT_NAME commands)
BOT_NAME=funny-telegram-bot
#Mensaar API key
MENSA_KEY=...
#list of id of the user that is given permission to restart the bot with /restart etc..
ADMIN=[...]

#OPTIONAL things for "hidden and/or undocumented commands":
#imgur image search
APIKEY_IMGUR=LGUYJgilubkj6R5
#google image search
GOOGLE_API=...
GOOGLE_CSE=...
```

# Running :

`./run.sh` will run in an endless loop. If you like systemd you can use the servicefile to have the bot start at system start.
