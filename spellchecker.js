const helpers = require('./helpers.js');

const userDicPath = './userDic.json';
const userDic = helpers.loadJsonFile(userDicPath);
const dictionary = helpers.loadJsonFile('./samisDictionary.json');

require('dotenv').config(); // for apikeys

const spellchecker = {
    run: (bot) => {
        helpers.registerCleaner(() => {
            const toWrite = JSON.stringify(userDic);
            helpers.writeJsonFile(userDicPath, toWrite);
        });

        bot.hears(new RegExp(`correct(@${process.env.BOT_NAME})? ([^ ]+) => (.*)`, 'i'), ({ match, replyWithMarkdown }) => {
            if (helpers.invalidSize(match[2]) || helpers.invalidSize(match[3])) {
                return;
            }
            userDic[match[2].toLowerCase()] = match[3].replace(/`/g, '\\`');
            replyWithMarkdown(`Change Saved : Try it out !\n\`/check@${process.env.BOT_NAME} ${match[2]}\``);
        });


        bot.hears(new RegExp(`check(@${process.env.BOT_NAME})? (.+)`, 'i'), ({ match, replyWithMarkdown }) => {
            if (helpers.invalidSize(match[2])) {
                return;
            }
            const input = match[2].toLowerCase().replace(/`/g, '\\`').split(' ');
            let hasChange = false;

            const output = input.map((el) => {
                if (userDic[el]) {
                    hasChange = true;
                    return userDic[el];
                } else if (dictionary[el]) {
                    hasChange = true;
                    return dictionary[el];
                }
                return el;
            });

            if (hasChange) {
                replyWithMarkdown(`Meinten Sie etwa : ${output.join(' ')}?`);
                return;
            }
            replyWithMarkdown(`Dies erscheint mir richtig. Falls nicht :\n\`/correct@${process.env.BOT_NAME} ${match[2]} => neues Wort\``);
        });
    },
};

module.exports = spellchecker;
