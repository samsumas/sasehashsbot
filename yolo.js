const helpers = require('./helpers.js');

module.exports = {
    run: bot =>
        bot.hears(/^\/(.)olo$/, ({ match, reply }) => {
            if (helpers.invalidSize(match[1])) {
                return;
            }
            const tab = {
                s: 'Sami',
                c: 'Carl',
                j: 'Jeremias',
                p: 'Pascal',
            };
            if (tab[match[1].toLowerCase()] != null) { reply(`${tab[match[1].toLowerCase()]} lebt nur einmal.`); return; }
            reply('You Only Live Online');
        }),
};
