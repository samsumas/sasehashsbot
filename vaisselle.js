const helpers = require('./helpers.js');
require('dotenv').config(); // for botname

const chooserOfVictims = bot => bot.hears(new RegExp(`(vaisselle|victim|spüler)(@${process.env.BOT_NAME})? (.*)`), ({ match, reply }) => {
    if (helpers.invalidSize(match[3])) {
        return;
    }
    const population = match[3].split(' ');
    const size = population.length;
    const victim = population[Math.floor(Math.random() * size)];
    // reply(`The chosen victim is ${victim}.\n Thank you for your sacrifice.`);
    reply(`${victim} darf heute spülen!\nViel Spaß dabei 😛`);
});

module.exports.run = chooserOfVictims;
