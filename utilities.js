const os = require('child_process');
const helpers = require('./helpers.js');
require('dotenv').config(); // for apikeys

const utilities = {
    run: (bot) => {
        const isAdmin = ctx => JSON.parse(process.env.ADMIN).includes(ctx.from.id);

        bot.command(helpers.appendName(['restart']), (ctx) => {
            if (isAdmin(ctx)) {
                ctx.reply('Restarting bot...');
                helpers.stopTheBot(bot);
            }
        });
        bot.command(helpers.appendName(['uptime', 'up']), ({ replyWithMarkdown }) => os.exec('uptime', (err, stdout) => replyWithMarkdown(`\`\`\`${stdout}\`\`\``)));
        bot.command(helpers.appendName(['getid']), (ctx) => {
            ctx.reply(`You are :${JSON.stringify(ctx.from)} from ${JSON.stringify(ctx.chat)}`);
        });

        bot.hears(/sudo(@[^ ]+)+ (.+)/, ({ match, reply }) => {
            if (helpers.invalidSize(match[2])) {
                return;
            }
            reply('Access granted.');
            reply(`Executing following command '${match[2]}' with administrator right.`);
            reply(match[1]);
            reply('Processing');
            setTimeout((() => {
                reply('...');
                setTimeout((() => {
                    reply('...');
                }), 2000);
                setTimeout((() => {
                    reply('...');
                }), 2000);
            }), 2000);
            setTimeout(() => {
                reply('Error detected. Trying to recover data.');
                setTimeout(() => reply('Failure. System destroyed', 2000));
            }, 9000);
        });
    },
};
module.exports = utilities;
