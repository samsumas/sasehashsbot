const helpers = require('./helpers.js');

require('dotenv').config(); // for apikeys

const googleCache = [];
const paradise = []; // contains a lot of paradisiac things

const images = {
    run: (bot) => {
        // sends the images
        const imgurAlbumHelper = (curr, ctx) => {
            // is album?
            if (curr === undefined) {
                ctx.reply('Nothing found!');
                return;
            }
            if (curr.is_album) {
                curr.images.forEach((e) => {
                    if (e.animated) { setTimeout(() => ctx.replyWithVideo(e.mp4), 500); } else { setTimeout(() => ctx.replyWithPhoto(e.link), 500); }
                });
            } else if (curr.animated) { ctx.replyWithVideo(curr.mp4); } else { ctx.replyWithPhoto(curr.link); }
        };

        // paradise[query] contains 3 fields :
        // pos (which image are we on)
        // page (actual page number)
        // json (api output)
        const paradiseHelper = (q, ctx) => {
            let query = q;
            if (query === undefined) {
                query = 'burger';
            }
            query = query.toLowerCase();
            const reply = ctx.reply;
            const sort = 'top';
            if (paradise[query]) {
                const curr = paradise[query];
                if (curr.json.data[curr.pos].link) {
                    imgurAlbumHelper(paradise[query].json.data[curr.pos], ctx); // send photos
                    paradise[query].pos++;
                } else {
                    // download next page
                    paradise[query].page++;
                    helpers.downloadToString(
                        `https://api.imgur.com/3/gallery/search/${sort}/month/${paradise[query].page}?q=${query}`,
                        { authorization: `Client-ID ${process.env.APIKEY_IMGUR}` },
                    ).then((res) => {
                        paradise[query].pos = 1;
                        paradise[query].json = JSON.parse(res);
                        imgurAlbumHelper(paradise[query].json.data[0], ctx); // send photos
                    }).error((err) => {
                        reply(`Nothing found :/${JSON.stringify(err)}`);
                    });
                }
            } else {
            // download json from api
                helpers.downloadToString(
                    `https://api.imgur.com/3/gallery/search/${sort}/month/0?q=${query}`,
                    { authorization: `Client-ID ${process.env.APIKEY_IMGUR}` },
                ).then((res) => {
                    paradise[query] = { json: JSON.parse(res), pos: 1, page: 0 }; // add new entry
                    imgurAlbumHelper(paradise[query].json.data[0], ctx); // output the images
                });
            }
        };

        // paradise[query] contains 3 fields :
        // pos (which image are we on)
        // page (actual page number)
        // json (api output)
        bot.hears(new RegExp(`/((.+)paradise(@${process.env.BOT_NAME})?)`, 'i'), ctx => paradiseHelper(ctx.match[2], ctx));
        bot.command(helpers.appendName(['lennysdeath']), ctx => paradiseHelper('burger', ctx));

        const googleAPICall = (q, start, callback) => {
            helpers.downloadToString(`https://www.googleapis.com/customsearch/v1?key=${process.env.GOOGLE_API}&cx=${process.env.GOOGLE_CSE}&q=${q}&prettyPrint=false&searchType=image&start=${start}`)
                .then(callback, (err) => { throw JSON.stringify(err); });
        };

        const googleImageSearch = (q, ctx) => {
            if (!googleCache[q]) {
                googleAPICall(q, 1, (res) => {
                    googleCache[q] = { json: JSON.parse(res), pos: 0, start: 1 };
                    ctx.replyWithPhoto(googleCache[q].json.items[googleCache[q].pos++].link);
                });
            } else {
                const currPos = ++googleCache[q].pos;
                const val = googleCache[q].json.items[currPos];
                if (!val) {
                    // loads next page
                    googleCache[q].start = currPos + 1;
                    googleCache[q].pos = 0;
                    googleAPICall(q, googleCache[q].start, (res) => {
                        googleCache[q].json = JSON.parse(res);
                        ctx.replyWithPhoto(googleCache[q].json.items[googleCache[q].pos].link);
                    });
                } else {
                    ctx.replyWithPhoto(val.link);
                }
            }
        };

        bot.hears(new RegExp(`^/((google)|(image))(@${process.env.BOT_NAME})? (.*)`, 'i'), (ctx) => {
            if (ctx.match[5]) googleImageSearch(ctx.match[5], ctx);
        });
    },
};
module.exports = images;
