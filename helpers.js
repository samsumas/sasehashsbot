const fs = require('fs');
const https = require('https');
const http = require('http');
require('dotenv').config(); // for botname

// transform ['test','b'] in ['test','test@botname','b','b@botname']
const appendName = arr => arr.concat(arr.map(x => `${x}@${process.env.BOT_NAME}`));

const writeJsonFile = (name, value) => {
    fs.writeFileSync(name, value);
};

const loadJsonFile = (name, defaultValue = '{}') => {
    try {
        return fs.readFileSync(name, 'utf-8');
    } catch (err) {
        fs.writeFileSync(name, defaultValue);
        return JSON.parse(defaultValue);
    }
};

const downloadToString = (url, headers = {}) => new Promise((succ, err) => {
    const protocol = url.startsWith('http:') ? http : https;
    protocol.get(url, { headers }, (res) => {
        let data = '';
        res.setEncoding('utf8');
        res.on('data', (chunk) => { data += chunk; });
        res.on('end', () => succ(data));
    }).on('error', err);
});

const maxChars = 100;
const invalidSize = str => str.length > maxChars;
const cleaners = [];

// be aware that the cleaners are not executed when the bot crashes, only when it is restarted
const registerCleaner = cleaner => cleaners.push(cleaner);

// because the stopcallback doesnt work :(
const stopTheBot = (bot) => {
    cleaners.forEach(c => c());
    bot.stop();
};

module.exports = { appendName, loadJsonFile, invalidSize, registerCleaner, stopTheBot, writeJsonFile, downloadToString };
