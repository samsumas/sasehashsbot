#!/bin/bash

function run {
    echo "running in :$(pwd) by ${USER}(${UID})"
    node server.js
    git pull
    git submodule update --init
    npm install  # make sure dependencies are up to date
    node clean.js

    run
}

run