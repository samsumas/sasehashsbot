const timer = {
    run: (bot) => {
        bot.hears(new RegExp(`^/(wecker)(@${process.env.BOT_NAME})? ([0-2]?[0-9]):([0-5][0-9]) ?(.*)`, 'i'), (ctx) => {
            const time = parseInt(ctx.match[3] * 60, 10) + parseInt(ctx.match[4], 10);
            const today = new Date();
            const todayMin = (today.getHours() * 60) + today.getMinutes();
            let wait = time - todayMin;
            if (time < todayMin) {
                wait = (1440 + time) - todayMin;
            }
            if (wait === 1) {
                ctx.reply(`${ctx.message.from.first_name}s Wecker klingelt in einer Minute`);
            } else {
                ctx.reply(`${ctx.message.from.first_name}s Wecker klingelt in ${wait} Minuten`);
            }
            setTimeout(() => { ctx.reply(`${ctx.message.from.first_name}s Wecker ${ctx.match[5]} klingelt!`); }, wait * 60 * 1000);
        });

        bot.hears(new RegExp(`^/(tea|tee|timer)(@${process.env.BOT_NAME})? ?([0-9]*)`, 'i'), (ctx) => {
            let time;
            if (ctx.match[3] > 70000) {
                ctx.reply('This is too much for me, try with less time (expressed in minutes)');
                return;
            }
            if (!ctx.match[3]) {
                time = 3;
            } else {
                time = ctx.match[3];
            }
            ctx.reply(`Starting ${time} minute tea timer...`);
            setTimeout(() => { ctx.reply(`${ctx.message.from.first_name}, your ${time} minute tea is ready!`); }, time * 60 * 1000);
        });
    },
};

module.exports = timer;
