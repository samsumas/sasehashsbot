const emojiRegex = require('emoji-regex');

require('dotenv').config();

// Minimal number of required policies.
const policyCount = 1;

const policyRegexp = emojiRegex();

// The actual policy checking.
const respectsPolicy = (message) => {
    const matches = message.match(policyRegexp);
    return matches && matches.length >= policyCount;
};

// Apply the policy to all images.
const moderate = (bot) => {
    bot.on('text', (ctx) => {
        // console.log(ctx.message);
        if (!respectsPolicy(ctx.message.text)) {
            const answer = `⚠️${ctx.message.from.first_name}'s message has been removed by Automod⚠️\nMessage : ,,${ctx.message.text}''\nReason : Policy ,,automod_policy'' not respected : Insufficient number of smileys!👿😡🤬`;
            ctx.deleteMessage().then(x => (x ? ctx.reply(answer) : x));
        }
    });
};

const automod = {
    run: moderate,
};

module.exports = automod;
