const mensalib = require('./mensaarformatter/lib.js');
const helpers = require('./helpers.js');
require('dotenv').config();

const makeBold = x => `<b>${x}</b>`;
const mensamenu = index => new Promise((success) => {
    mensalib.mensamenu(index, process.env.MENSA_KEY, x => `<b>${x}</b>`).then(res => success(res.join('\n')));
});

const mensamenuCommand = index => (ctx => mensamenu(index).then(ctx.replyWithHTML));

const schnitzel = index => new Promise((success) => {
    mensalib.schnitzel(index, process.env.MENSA_KEY, x => `<b>${x}</b>`).then(res => success(res.join('\n')));
});

const schnitzelCommand = index => (ctx => schnitzel(index).then(ctx.replyWithHTML));

const MAX_DAYS_IN_FUTURE = 3;

const for2 = (f, offset) => index => () => f(index, process.env.MENSA_KEY, makeBold).then(menu => ({
    type: 'article',
    id: (offset * MAX_DAYS_IN_FUTURE) + index,
    title: menu[0].replace(':', ''),
    input_message_content: {
        message_text: menu.join('\n'),
        parse_mode: 'HTML',
    },
}));

const menufor2 = for2(mensalib.mensamenu, 0);
const schnitzelfor2 = for2(mensalib.schnitzel, 1);

const mensa = {
    run: (bot) => {
        bot.command(helpers.appendName(['mensa']), mensamenuCommand(0));
        bot.command(helpers.appendName(['mensa2morrow']), mensamenuCommand(1));
        bot.command(helpers.appendName(['mensa3morrow']), mensamenuCommand(2));
        bot.command(helpers.appendName(['schnitzel']), schnitzelCommand(0));
    },
    registerQueryHandler: (handlerList) => {
        for (let i = 0; i < MAX_DAYS_IN_FUTURE; i++) {
            handlerList.push(menufor2(i));
        }
        for (let i = 0; i < MAX_DAYS_IN_FUTURE; i++) {
            handlerList.push(schnitzelfor2(i));
        }
    },
    menu: mensamenu(0),
};

module.exports = mensa;
